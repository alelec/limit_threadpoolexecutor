========================
limit_threadpoolexecutor
========================

THis package live patches concurrent.futures.ThreadPoolExecutor to limit default max_workers.

On my shared remote server, the deafult max_workers setting of `num_cpu * 5` results in a huge number of threads that hit the limit per user too quickly.

Simply install with::

  pip install limit_threadpoolexecutor

and every package/program using that copy of python should automatically have their max_threads setting limited to 10


Acknowledgements
----------------
The method of patching at runtime is built from the autowrapt module: https://pypi.python.org/pypi/autowrapt