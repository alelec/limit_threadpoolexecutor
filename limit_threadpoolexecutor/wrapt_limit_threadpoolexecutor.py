from __future__ import absolute_import
import wrapt


# pip master has most commands moved into _internal folder
@wrapt.when_imported('concurrent.futures.thread')
def apply_patches(thread):
    override_threadpoolexecutor(thread)


def override_threadpoolexecutor(thread):
    class ThreadPoolExecutor(thread.ThreadPoolExecutor):
        """Override and limit max_workers."""

        def __init__(self, *args, max_workers=None, **kwargs):
            if max_workers is None or max_workers > 10:
                max_workers = 10
            super().__init__(*args, max_workers=max_workers, **kwargs)

    thread.ThreadPoolExecutor = ThreadPoolExecutor
