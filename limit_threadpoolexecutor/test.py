import unittest
from concurrent.futures import ThreadPoolExecutor


class TestLimit(unittest.TestCase):

    def test_default_num_workers(self):
        e = ThreadPoolExecutor()
        assert e._max_workers == 10, "was: %s" % e._max_workers

    def test_max_num_workers(self):
        e = ThreadPoolExecutor(max_workers=20)
        assert e._max_workers == 10, "was: %s" % e._max_workers

    def test_normal_num_workers(self):
        e = ThreadPoolExecutor(max_workers=2)
        assert e._max_workers == 2, "was: %s" % e._max_workers


if __name__ == '__main__':
    unittest.main()
